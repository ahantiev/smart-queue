package smart.queue;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class OrderedTaskExecutorTest {
    @Test
    public void executeAllWithSuccess() throws Exception {
        OrderedTaskExecutor executor = new OrderedTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(100);
        executor.initialize();

        executor.submit(createTask(1, "task1"));
        executor.submit(createTask(2, "task1"));
        executor.submit(createTask(3, "task1"));
        executor.submit(createTask(4, "task2"));
        executor.submit(createTask(5, "task1"));
        executor.submit(createTask(6, "task2"));

        Thread.sleep(3000);

        TaskQueueWrapper wrapperForTask1 = executor.getTaskQueueMap().get("task1");
        assertEquals(wrapperForTask1.getFinishedTasksCounter().get(), 4);
        assertEquals(wrapperForTask1.getTotalTasksCounter().get(), 4);
        assertEquals(wrapperForTask1.getStatus(), TaskQueueWrapper.Status.WORKED);
        assertEquals(wrapperForTask1.getFinishedTaskIds(), Arrays.asList(1, 2, 3, 5));

        TaskQueueWrapper wrapperForTask2 = executor.getTaskQueueMap().get("task2");
        assertEquals(wrapperForTask2.getFinishedTasksCounter().get(), 2);
        assertEquals(wrapperForTask2.getTotalTasksCounter().get(), 2);
        assertEquals(wrapperForTask2.getStatus(), TaskQueueWrapper.Status.WORKED);
        assertEquals(wrapperForTask2.getFinishedTaskIds(), Arrays.asList(4, 6));
    }

    @Test
    public void executeWithFailedTask() throws Exception {
        OrderedTaskExecutor executor = new OrderedTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(100);
        executor.initialize();

        executor.submit(createTask(1, "task1"));
        executor.submit(createTask(2, "task1"));
        executor.submit(createFailedTask(3, "task1"));
        executor.submit(createTask(4, "task2"));
        executor.submit(createTask(5, "task1"));
        executor.submit(createTask(6, "task2"));

        Thread.sleep(3000);

        TaskQueueWrapper wrapperForTask1 = executor.getTaskQueueMap().get("task1");
        assertEquals(wrapperForTask1.getFinishedTasksCounter().get(), 2);
        assertEquals(wrapperForTask1.getTotalTasksCounter().get(), 4);
        assertEquals(wrapperForTask1.getStatus(), TaskQueueWrapper.Status.FAILED);
        assertEquals(wrapperForTask1.getFinishedTaskIds(), Arrays.asList(1, 2));

        TaskQueueWrapper wrapperForTask2 = executor.getTaskQueueMap().get("task2");
        assertEquals(wrapperForTask2.getFinishedTasksCounter().get(), 2);
        assertEquals(wrapperForTask2.getTotalTasksCounter().get(), 2);
        assertEquals(wrapperForTask2.getStatus(), TaskQueueWrapper.Status.WORKED);
        assertEquals(wrapperForTask2.getFinishedTaskIds(), Arrays.asList(4, 6));
    }

    private static Task createTask(Integer id, String key) {
        return new Task() {
            @Override
            public int getId() {
                return id;
            }

            @Override
            public String getKey() {
                return key;
            }

            @Override
            public void run() {
                long randomDelay = 200 + (long) (Math.random() * (1 - 200));
                try {
                    Thread.sleep(randomDelay);
                } catch (InterruptedException e) {
                    throw new RuntimeException("Task: " + getKey() + " is failed!", e);
                }
            }
        };
    }

    private static Task createFailedTask(Integer id, String key) {
        return new Task() {
            @Override
            public int getId() {
                return id;
            }

            @Override
            public String getKey() {
                return key;
            }

            @Override
            public void run() {
                throw new RuntimeException("Task: " + getKey() + " is failed!");
            }
        };
    }
}
