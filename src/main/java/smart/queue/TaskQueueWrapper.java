package smart.queue;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaskQueueWrapper {
    private static final Logger LOG = Logger.getLogger(TaskQueueWrapper.class.getName());

    private final Queue<Task> queue = new LinkedBlockingQueue<>();
    private final List<Integer> finishedTaskIds = new ArrayList<>();
    private final AtomicInteger totalTasksCounter = new AtomicInteger();
    private final AtomicInteger finishedTasksCounter = new AtomicInteger();
    private final String taskKey;
    private NextHandler nextHandler;
    private Status status = Status.WORKED;

    public TaskQueueWrapper(String taskKey, NextHandler nextHandler) {
        this.taskKey = taskKey;
        this.nextHandler = nextHandler;
    }

    public void add(Task task) {
        int totalCounter = totalTasksCounter.incrementAndGet();

        if (totalCounter > finishedTasksCounter.get() + 1) {
            queue.add(task);
        } else {
            nextHandler.run(createCallbackTask(this, task));
        }
    }

    private void runNext() {
        Task nextTask = queue.poll();
        if (nextTask == null) {
            return;
        }

        nextHandler.run(createCallbackTask(this, nextTask));
    }

    public boolean isWorked() {
        return status.equals(TaskQueueWrapper.Status.WORKED);
    }

    public AtomicInteger getTotalTasksCounter() {
        return totalTasksCounter;
    }

    public AtomicInteger getFinishedTasksCounter() {
        return finishedTasksCounter;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Integer> getFinishedTaskIds() {
        return finishedTaskIds;
    }

    private CallbackTask createCallbackTask(TaskQueueWrapper queueWrapper, Task task) {
        return new CallbackTask(task, new Callback() {
            @Override
            public void onSuccess(Task task) {
                finishedTaskIds.add(task.getId());
                queueWrapper.getFinishedTasksCounter().incrementAndGet();
                queueWrapper.runNext();

                logStatistics(task);
            }

            @Override
            public void onError(Task task, Throwable error) {
                queueWrapper.setStatus(Status.FAILED);
                LOG.log(Level.SEVERE, "Task is error. Key: " + task.getKey(), error);
                logStatistics(task);
            }
        });
    }

    public void logStatistics() {
        logStatistics(null);
    }

    private void logStatistics(Task task) {
        StringBuilder builder = new StringBuilder();
        if (task != null) {
            builder.append("ID:").append(task.getId());
        }
        builder.append(" Key: ").append(taskKey);
        builder.append(" Finished: ").append(finishedTasksCounter.get()).append("/").append(totalTasksCounter.get());
        builder.append(" Status: ").append(status);

        LOG.info(builder.toString());
    }

    interface NextHandler {
        void run(CallbackTask task);
    }

    enum Status {
        WORKED, FAILED, REJECTED
    }
}
