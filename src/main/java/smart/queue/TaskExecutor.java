package smart.queue;

public interface TaskExecutor {
    void submit(Task task);
}
