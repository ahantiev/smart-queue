package smart.queue;

public interface Task {
    String getKey();
    int getId();
    void run();
}
