package smart.queue;

class CallbackTask implements Runnable {

    private final Task task;

    private final Callback callback;

    CallbackTask(Task task, Callback callback) {
        this.task = task;
        this.callback = callback;
    }

    public void run() {
        try {
            task.run();
            callback.onSuccess(task);
        } catch (Exception e) {
            callback.onError(task, e);
        }
    }

    public Task getTask() {
        return task;
    }
}