package smart.queue;

public interface Callback {
    void onSuccess(Task task);
    void onError(Task task, Throwable error);
}
