package smart.queue;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class OrderedTaskExecutor implements TaskExecutor {

    private static final Logger LOG = Logger.getLogger(OrderedTaskExecutor.class.getName());

    private int corePoolSize = 1;
    private int maxPoolSize = 100;
    private int keepAliveSeconds = 60;
    private int queueCapacity = 1000;

    private ThreadPoolExecutor executor;
    private final Map<String, TaskQueueWrapper> taskQueueMap = new HashMap<>();

    public void initialize() {
        BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(queueCapacity);

        ThreadFactory threadFactory = Executors.defaultThreadFactory();

        executor = new ThreadPoolExecutor(
                this.corePoolSize, this.maxPoolSize, this.keepAliveSeconds, TimeUnit.SECONDS,
                queue, threadFactory, getRejectedExecutionHandler());
    }

    public void submit(Task task) {
        synchronized (taskQueueMap) {
            TaskQueueWrapper queueWrapper = taskQueueMap.get(task.getKey());
            if (queueWrapper == null) {
                queueWrapper = new TaskQueueWrapper(task.getKey(), next -> executor.execute(next));
                taskQueueMap.put(task.getKey(), queueWrapper);
            }

            if (!queueWrapper.isWorked()) {
                queueWrapper.logStatistics();
            }

            queueWrapper.add(task);
        }
    }

    private RejectedExecutionHandler getRejectedExecutionHandler() {
        return (runnable, executor) -> {
            synchronized (taskQueueMap) {
                String key = ((CallbackTask) runnable).getTask().getKey();
                LOG.info("Task: " + key +" was rejected" );

                // TODO: may be need retry logic
                TaskQueueWrapper queueWrapper = taskQueueMap.get(key);
                if(queueWrapper != null) {
                    queueWrapper.setStatus(TaskQueueWrapper.Status.REJECTED);
                }
            }
        };
    }


    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public int getKeepAliveSeconds() {
        return keepAliveSeconds;
    }

    public void setKeepAliveSeconds(int keepAliveSeconds) {
        this.keepAliveSeconds = keepAliveSeconds;
    }

    public int getQueueCapacity() {
        return queueCapacity;
    }

    public Map<String, TaskQueueWrapper> getTaskQueueMap() {
        return taskQueueMap;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    public ThreadPoolExecutor getExecutor() {
        return executor;
    }
}
