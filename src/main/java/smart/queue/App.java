package smart.queue;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class App {
    private static final Logger LOG = Logger.getLogger(App.class.getName());

    private static AtomicInteger taskCounter;

    public static void main(String[] args) throws InterruptedException {
        OrderedTaskExecutor executor = new OrderedTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(100);
        executor.initialize();

        List<String> keys = IntStream.rangeClosed(0, 2)
                .mapToObj(value -> "key" + value)
                .collect(Collectors.toList());

        taskCounter = new AtomicInteger();

        while (true) {
            IntStream.rangeClosed(1, 10).forEach(value -> {
                int randomIndex = new Random().nextInt(keys.size());
                String key = keys.get(randomIndex);
                executor.submit(createTask(key));
            });

            Thread.sleep(1000);
        }

    }

    private static Task createTask(String key) {
        int counter = taskCounter.incrementAndGet();
        return new Task() {
            @Override
            public int getId() {
                return counter;
            }

            @Override
            public String getKey() {
                return key;
            }

            @Override
            public void run() {
                long randomDelay = 1000 + (long) (Math.random() * (1 - 1000));
                try {
                    Thread.sleep(randomDelay);
                } catch (InterruptedException e) {
                    throw new RuntimeException("Error in task: " + getKey(), e);
                }

                if (getId() % 15 == 0 && getKey().equals("key0")) {
                    throw new RuntimeException("Error in task: " + getKey());
                }
            }
        };
    }
}
